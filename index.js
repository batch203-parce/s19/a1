//console.log("Hello World");

let username;
let password;
let role;

function logInFunction() {
	username = prompt("Enter your username:");
	password = prompt("Enter your password:");
	role = prompt("Enter your role:");
	
	if (
		username === null || username === " " || 
		password === null || password === " " || 
		role === null || role === " "
		) 
	{
		alert("The input should not be empty");
		logInFunction();
	}
	else {
		switch(role){
		case "admin":
			alert("Welcome back to the class portal, admin!");
			break;
		case "teacher":
			alert("Thank you for logging in, teacher!");
			break;
		case "student":
			alert("Welcome to the class portal, student!");
			break;
		default:
			alert("Role out of range.");
			logInFunction();
		}
	}

}
logInFunction();


function getAverage(num1, num2, num3, num4){
	return (num1 + num2 + num3 + num4) / 4;
}

function checkAverage(){
	if (avg < 74){
	console.log("Hello, student, your average is " + avg +". The letter equivalent is F");
	}
	else if (avg >= 75 && avg <= 79){
		console.log("Hello, student, your average is " + avg +". The letter equivalent is D");
	}
	else if (avg >= 80 && avg <= 84){
		console.log("Hello, student, your average is " + avg +". The letter equivalent is C");
	}
	else if (avg >=85 && avg <= 89){
		console.log("Hello, student, your average is " + avg +". The letter equivalent is B");
	}
	else if (avg >=90 && avg <= 95){
		console.log("Hello, student, your average is " + avg +". The letter equivalent is A");
	}
	else if (avg >=96 || avg <= 100){
		console.log("Hello, student, your average is " + avg +". The letter equivalent is A+");
	}
	else {
		console.log("You don't have any grades yet");
	}
}

let avg = Math.round(getAverage(71, 70, 73, 74));
console.log("checkAverage (71, 70, 73, 74)");
checkAverage();

avg = Math.round(getAverage(75, 75, 76, 78));
console.log("checkAverage (75, 75, 76, 78)");
checkAverage();

avg = Math.round(getAverage(80, 81, 82, 78));
console.log("checkAverage (80, 81, 82, 78)");
checkAverage();

avg = Math.round(getAverage(84, 85, 87, 88));
console.log("checkAverage (84, 85, 87, 88)");
checkAverage();

avg = Math.round(getAverage(89, 90, 91, 90));
console.log("checkAverage (89, 90, 91, 90)");
checkAverage();

avg = Math.round(getAverage(91, 96, 97, 99));
console.log("checkAverage (91, 96, 97, 99)");
checkAverage();